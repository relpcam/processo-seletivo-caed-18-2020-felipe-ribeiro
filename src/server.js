
const app = require('./app');

app.set('view engine', 'ejs');
app.set('views', './src/views');

app.listen(3333); 