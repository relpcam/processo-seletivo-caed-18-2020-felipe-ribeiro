const mongoose = require('mongoose');
const { Schema, model } = mongoose;

const AlunoSchema = new Schema({
    nome:{
        type: String,
        required: true, 
        max: 100
    },
    cpf:{
        type: String,
        required: true, 
        max: 11,
        unique: true
    },
    data_nascimento:{
        type: Date,
        required: true, 
    },
    sexo:{
        type: String,
        required: true, 
    },
    email:{
        type: String,
         
    },
    maior_idade:{
        type: String
    },
    menor_idade:{
        type: String
    },
    data_criacao:{
        type: Date
    }
    
    
    

});
//nome, cpf, data_nascimento, sexo, email
module.exports = model('Aluno', AlunoSchema);