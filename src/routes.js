const {Router} = require('express');
const {uuid} = require('uuidv4');
const AlunoController = require('./Controllers/AlunoController');
const homeController = require('./Controllers/homeController')
const { body, validationResult } = require('express-validator');

const routes = Router();

routes.get('/',function(request,response){

    
    homeController.index(request,response);
});


routes.post('/addaluno', [
    body('nome', 'O campo NOME exedeu o máximo permitido').notEmpty().isLength({ max: 100 }),
    body('cpf', 'CPF São permitidos apenas 11 caracters').notEmpty().isLength({ max: 11 }),
    body('data_nascimento', 'DATA DE NASCIMENTO é obrigatório').notEmpty(),
    body('sexo', 'O campo SEXO é obrigatório').notEmpty(),
    body('email', 'Email inválido').isEmail().notEmpty()    

  ], (req, res) => {
    // Finds the validation errors in this request and wraps them in an object with handy functions
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.render('home', {erros: errors.array()})
    //   return res.status(400).json({ errors: errors.array() });
    }
  
    AlunoController.store(req,res);
    // User.create({
    //   username: req.body.username,
    //   password: req.body.password
    // }).then(user => res.json(user));
  });



// routes.post('/addaluno',function(request,response){
//     AlunoController.store(request,response);
// });

//routes.post('/addaluno', AlunoController.store);


module.exports = routes; 