
const express = require('express');
const routes = require('./routes');
var bodyParser = require('body-parser');

import './database';


const app = express();


//Configuração para transformar formulário em json
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}))


app.use(express.json());
app.use(routes);

module.exports = app;