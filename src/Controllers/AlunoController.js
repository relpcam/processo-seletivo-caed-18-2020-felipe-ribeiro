const Aluno = require('../models/Aluno');
const { body, validationResult } = require('express-validator');



class AlunoController{

    //Criar aluno
    async store(request, response){
        const {nome, cpf, data_nascimento, sexo, email} = request.body;
        
        
        //Fução calcular idade
        function calcIdade(ano_aniversario, mes_aniversario, dia_aniversario) {
            var d = new Date,
                ano_atual = d.getFullYear(),
                mes_atual = d.getMonth() + 1,
                dia_atual = d.getDate(),
        
                ano_aniversario = +ano_aniversario,
                mes_aniversario = +mes_aniversario,
                dia_aniversario = +dia_aniversario,
        
                quantos_anos = ano_atual - ano_aniversario;
        
            if (mes_atual < mes_aniversario || mes_atual == mes_aniversario && dia_atual < dia_aniversario) {
                quantos_anos--;
            }
        
            return quantos_anos < 0 ? 0 : quantos_anos;
        }
        
        const dataNascimento = data_nascimento.split("-");
        
        
        
        const idadeAluno = calcIdade(dataNascimento[0], dataNascimento[1], dataNascimento[2]);
        

        var calc_maior_idade = '';

        if (idadeAluno >= 18){
            calc_maior_idade = 'sim';
            
        }else{
            calc_maior_idade = 'não';
            
        }


        function dataAtualFormatada(){
            var data = new Date(),
                dia  = data.getDate().toString().padStart(2, '0'),
                mes  = (data.getMonth()+1).toString().padStart(2, '0'), //+1 pois no getMonth Janeiro começa com zero.
                ano  = data.getFullYear(),
                hora = data.getHours(),
                minuto = data.getMinutes(),
                segundos = data.getSeconds();
            return ano+"-"+mes+"-"+dia+" "+hora+":"+minuto+":"+segundos;
        }

        //Verifica se o aluno existe
        const alunoExists = await Aluno.findOne({cpf});

        
        //Verifica se existe o aluno. Caso exista atualiza somente data_alteracao
        if (alunoExists){
            
            const aluno = await Aluno.update({
                nome:alunoExists.nome, cpf:alunoExists.cpf, data_nascimento:alunoExists.data_nascimento, sexo:alunoExists.sexo, email:alunoExists.email, maior_idade: alunoExists.maior_idade, data_criacao: alunoExists.data_criacao, data_alteracao: dataAtualFormatada()
            });
            return response.json(aluno);
        }

        //ADD novo aluno
        const aluno = await Aluno.create({
            nome, cpf, data_nascimento, sexo, email, maior_idade: calc_maior_idade, data_criacao: dataAtualFormatada()
        });
        
        

        // return response.status(400).send();

        return response.json(aluno);
        

        // return response.json(aluno);

    }
}
module.exports = new AlunoController();