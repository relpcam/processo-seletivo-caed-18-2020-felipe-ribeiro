## Processo seletivo 19-2020 | CAED

* Endpoint para receber dados de alunos e persitir em um banco de dados.


* Projeto desenvolvido em Nodejs + Express.

* View Engine: EJS


## Getting Started

Clonar ou baixar o repositório.

### Pré requisites

This is an example of how to list things you need to use the software and how to install them.

* npm
  ```sh
  npm install npm@latest -g
  ```

* yarn
  ```sh
  npm install -g yarn
  ```
  
* docker
  ```sh
  docker pull tutum/mongodb
  ```
    
* nodemon
  ```sh
  npm install -g nodemon
  ```
  
  ### Instalação
  
  1. Clone o repositório
     ```sh
     git clone https://relpcam@bitbucket.org/relpcam/processo-seletivo-caed-18-2020-felipe-ribeiro.git
     ```
  2. Install NPM packages
     ```sh
     npm install
     ```
  3. Configuração Container/Mongodb
       ```sh
       docker run --name caedmongodb -d -p 27017:27017 -p 28017:28017 -e AUTH=no tutum/mongodb
       ```
     
  ## Como usar
  
  Executar o comando no terminal
  ```sh
    yarn dev
   ```
  
  A aplicação está configurado na porta 3333
  ```sh
      http://localhost:3333
     ``` 

## Contact

Felipe Ribeiro - relpcam@gmail.com

Project Link: [https://bitbucket.org/relpcam/processo-seletivo-caed-18-2020-felipe-ribeiro/](https://bitbucket.org/relpcam/processo-seletivo-caed-18-2020-felipe-ribeiro/)

